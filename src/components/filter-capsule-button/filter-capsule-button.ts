import { Component } from '@angular/core';

/**
 * Generated class for the FilterCapsuleButtonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'filter-capsule-button',
  templateUrl: 'filter-capsule-button.html'
})
export class FilterCapsuleButtonComponent {

  text: string;

  constructor() {
    console.log('Hello FilterCapsuleButtonComponent Component');
    this.text = 'Hello World';
  }

}
