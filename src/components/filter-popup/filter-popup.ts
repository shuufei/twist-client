import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

import { FilterProvider } from '../../providers/filter/filter';

/**
 * Generated class for the FilterPopupComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'filter-popup',
  templateUrl: 'filter-popup.html'
})
export class FilterPopupComponent {

  constructor(
    private viewCtrl: ViewController,
    private filterService: FilterProvider
  ) {}

  closePopup() {
    this.viewCtrl.dismiss();
  }

  changeFilter(filter: string) {
    this.filterService.changeFilter(filter);
    this.viewCtrl.dismiss();
  }
}
