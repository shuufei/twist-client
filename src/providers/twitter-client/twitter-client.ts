import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as TestData from '../../dummyData/data';

/*
  Generated class for the TwitterClientProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

const ENDPOINT = 'http://ec2-52-36-196-203.us-west-2.compute.amazonaws.com:3000';
// const TEST_ENDPOINT = 'http://localhost:3000/test';

@Injectable()
export class TwitterClientProvider {

  constructor(public http: HttpClient) {
    console.log('Hello TwitterClientProvider Provider');
  }

  getStatuses(listId, sinceId, maxId, count, includeRts) {
    return new Promise((resolve, reject) => {
      let Params = new HttpParams();
      if (!listId) { reject(new Error('listId is must parameter.')); }
      Params = Params.append('list_id', listId);
      Params = null == sinceId ? Params : Params.append('since_id', sinceId);
      Params = null == maxId ? Params : Params.append('max_id', maxId);
      Params = null == count ? Params : Params.append('count', count);
      Params = null == includeRts ? Params : Params.append('include_rts', includeRts);
      this.http.get(`${ENDPOINT}/lists/statuses`, { params: Params }).toPromise()
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          console.log('=== http error: ', err);
          // reject();
          resolve(TestData.default.tweets);
        });
    });
  }

  getLists(userId) {
    let Params = new HttpParams();
    if (userId) {
      Params = Params.append('user_id', userId);
    }
    return this.http.get(`${ENDPOINT}/lists/list`, { params: Params }).toPromise()
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log('=== http error: ', err);
        return TestData.default.lists;
      });
  }

  async postFavorites(tweetId: string, state: boolean) {
    let res;
    let reqBody = {
      id: tweetId,
      include_entities: true
    };
    if (state) {
      res = await this.http.post(`${ENDPOINT}/favorites/create`, reqBody).toPromise();
    } else {
      res = await this.http.post(`${ENDPOINT}/favorites/destroy`, reqBody).toPromise();
    }
    return res;
  }

  getFavoStatuses(listId: number, maxId: number) {
    let listUserIds = [];
    let favoStatuses = [];

    let favoParams = new HttpParams();
    let membersParams = new HttpParams();
    let tweetCount = 50;
    favoParams = favoParams.append('count', String(tweetCount));
    favoParams = null == maxId ? favoParams : favoParams.append('max_id', String(maxId));
    membersParams = membersParams.append('count', String(tweetCount));
    membersParams = membersParams.append('list_id', String(listId));
    let promises = [];
    promises.push(this.http.get(`${ENDPOINT}/favorites/list`, {params: favoParams}).toPromise());
    promises.push(this.http.get(`${ENDPOINT}/lists/members`, {params: membersParams}).toPromise());
    return Promise.all(promises)
      .then((res) => {
        favoStatuses = res[0];
        listUserIds = res[1].users.map((user) => {
          return user.id_str;
        });
        return favoStatuses.filter((status) => {
          return listUserIds.indexOf(status.user.id_str) !== -1;
        });
      })
      .catch((err) => {
        console.log('=== error ===', err);
        return TestData.default.tweets;
      });
  }

}
