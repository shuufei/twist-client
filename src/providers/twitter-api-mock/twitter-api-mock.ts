import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the TwitterApiMockProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TwitterApiMockProvider {

  constructor(public http: HttpClient) {
    console.log('Hello TwitterApiMockProvider Provider');
  }

  getLists() {
    let endpoint = 'http://localhost:3000/lists/list';
    this.http.get(endpoint).subscribe(data => {
      console.log('=== http req: ', data);
    });

    let lists = [
      {
        id: 948603034430406700,
        id_str: '948603034430406656',
        name: 'designer'
      },
      {
        id: 948603034430406700,
        id_str: '948603034430406656',
        name: '世界一周'
      },
      {
        id: 948603034430406700,
        id_str: '948603034430406656',
        name: 'AWS'
      }
    ];
    return Promise.resolve(lists);
  }

}
