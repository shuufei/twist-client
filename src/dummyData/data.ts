export default {
  tweets: [
    {
      "created_at": "Wed Jan 24 13:00:27 +0000 2018",
      "id": 956149699429494800,
      "id_str": "956149699429494784",
      "text": "How can large enterprise organizations transform using design principles? @aarron, @lizholz, @sherie, and @mcutler… https://t.co/rmGcEcHbWx",
      "truncated": true,
      "entities": {
        "hashtags": [],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "aarron",
            "name": "Aarron Walter",
            "id": 9463382,
            "id_str": "9463382",
            "indices": [
              74,
              81
            ]
          },
          {
            "screen_name": "lizholz",
            "name": "liz holz",
            "id": 41403923,
            "id_str": "41403923",
            "indices": [
              83,
              91
            ]
          },
          {
            "screen_name": "sherie",
            "name": "sherie masters",
            "id": 14325364,
            "id_str": "14325364",
            "indices": [
              93,
              100
            ]
          },
          {
            "screen_name": "mcutler",
            "name": "Matt Cutler",
            "id": 1901,
            "id_str": "1901",
            "indices": [
              106,
              114
            ]
          }
        ],
        "urls": [
          {
            "url": "https://t.co/rmGcEcHbWx",
            "expanded_url": "https://twitter.com/i/web/status/956149699429494784",
            "display_url": "twitter.com/i/web/status/9…",
            "indices": [
              116,
              139
            ]
          }
        ]
      },
      "source": "<a href=\"http://sproutsocial.com\" rel=\"nofollow\">Sprout Social</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 256093789,
        "id_str": "256093789",
        "name": "InVision",
        "screen_name": "InVisionApp",
        "location": "New York, NY",
        "description": "A Digital Product Design platform used to make the world’s \nbest customer experiences. Sign up free https://t.co/TVIUdSoBuA Need help? Hit up @invisionsupport",
        "url": "http://t.co/fRebRm4GXd",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/fRebRm4GXd",
                "expanded_url": "http://www.InVisionApp.com",
                "display_url": "InVisionApp.com",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": [
              {
                "url": "https://t.co/TVIUdSoBuA",
                "expanded_url": "http://invisionapp.com",
                "display_url": "invisionapp.com",
                "indices": [
                  100,
                  123
                ]
              }
            ]
          }
        },
        "protected": false,
        "followers_count": 119244,
        "friends_count": 5033,
        "listed_count": 2633,
        "created_at": "Tue Feb 22 17:02:10 +0000 2011",
        "favourites_count": 69117,
        "utc_offset": -18000,
        "time_zone": "Eastern Time (US & Canada)",
        "geo_enabled": true,
        "verified": true,
        "statuses_count": 43891,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/256093789/1509643240",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 2,
      "favorite_count": 6,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Wed Jan 24 01:57:29 +0000 2018",
      "id": 955982860049002500,
      "id_str": "955982860049002496",
      "text": "登録したコンポーネントをオーガナイゼーション内で共有できるようになりました🎉 プロトタイプをスムーズかつスピードアップして作成いただけます💪詳細はこちらのブログをご覧ください👀 #prott\nhttps://t.co/5nf9bzyqbI",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "prott",
            "indices": [
              89,
              95
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [],
        "urls": [
          {
            "url": "https://t.co/5nf9bzyqbI",
            "expanded_url": "https://blog.prottapp.com/post/ja/share-commonly-used-components-in-organization",
            "display_url": "blog.prottapp.com/post/ja/share-…",
            "indices": [
              96,
              119
            ]
          }
        ]
      },
      "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 2351535054,
        "id_str": "2351535054",
        "name": "Prott",
        "screen_name": "prott_jp",
        "location": "Tokyo",
        "description": "Prott（プロット）は簡単にアプリやWebのプロトタイプが作れて、チームでコミュニケーションができるプロトタイピングツール。UIデザインファームのGoodpatchが開発してます。皆さんのフィードバックをお待ちしてます！ \n\nEnglish tweets @prottapp",
        "url": "http://t.co/YSL1BD5g7g",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/YSL1BD5g7g",
                "expanded_url": "http://prottapp.com/ja/",
                "display_url": "prottapp.com/ja/",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 1248,
        "friends_count": 1041,
        "listed_count": 36,
        "created_at": "Wed Feb 19 11:22:37 +0000 2014",
        "favourites_count": 1790,
        "utc_offset": 32400,
        "time_zone": "Tokyo",
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 1557,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "3B3E57",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/507104043643068416/L1SGzsIo_normal.png",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/507104043643068416/L1SGzsIo_normal.png",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/2351535054/1413967210",
        "profile_link_color": "1B95E0",
        "profile_sidebar_border_color": "FFFFFF",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": false,
        "has_extended_profile": false,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 2,
      "favorite_count": 1,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "ja"
    },
    {
      "created_at": "Wed Jan 24 01:45:50 +0000 2018",
      "id": 955979928159342600,
      "id_str": "955979928159342593",
      "text": "【ワイヤーフレーム新機能】オーガナイゼーション内で共通コンポーネントを使えるようになりました🎉\nhttps://t.co/jCPwnkCR5Y #changelog",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "changelog",
            "indices": [
              72,
              82
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [],
        "urls": [
          {
            "url": "https://t.co/jCPwnkCR5Y",
            "expanded_url": "http://headwayapp.co/prott-ja-changelog/%E3%83%AF%E3%82%A4%E3%83%A4%E3%83%BC%E3%83%95%E3%83%AC%E3%83%BC%E3%83%A0%E6%96%B0%E6%A9%9F%E8%83%BD-%E3%82%AA%E3%83%BC%E3%82%AC%E3%83%8A%E3%82%A4%E3%82%BC%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3%E5%86%85%E3%81%A7%E5%85%B1%E9%80%9A%E3%82%B3%E3%83%B3%E3%83%9D%E3%83%BC%E3%83%8D%E3%83%B3%E3%83%88%E3%82%92%E4%BD%BF%E3%81%88%E3%82%8B%E3%82%88%E3%81%86%E3%81%AB%E3%81%AA%E3%82%8A%E3%81%BE%E3%81%97%E3%81%9F-46413",
            "display_url": "headwayapp.co/prott-ja-chang…",
            "indices": [
              48,
              71
            ]
          }
        ]
      },
      "source": "<a href=\"https://headwayapp.co\" rel=\"nofollow\">@helloheadway</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 2351535054,
        "id_str": "2351535054",
        "name": "Prott",
        "screen_name": "prott_jp",
        "location": "Tokyo",
        "description": "Prott（プロット）は簡単にアプリやWebのプロトタイプが作れて、チームでコミュニケーションができるプロトタイピングツール。UIデザインファームのGoodpatchが開発してます。皆さんのフィードバックをお待ちしてます！ \n\nEnglish tweets @prottapp",
        "url": "http://t.co/YSL1BD5g7g",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/YSL1BD5g7g",
                "expanded_url": "http://prottapp.com/ja/",
                "display_url": "prottapp.com/ja/",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 1248,
        "friends_count": 1041,
        "listed_count": 36,
        "created_at": "Wed Feb 19 11:22:37 +0000 2014",
        "favourites_count": 1790,
        "utc_offset": 32400,
        "time_zone": "Tokyo",
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 1557,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "3B3E57",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/507104043643068416/L1SGzsIo_normal.png",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/507104043643068416/L1SGzsIo_normal.png",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/2351535054/1413967210",
        "profile_link_color": "1B95E0",
        "profile_sidebar_border_color": "FFFFFF",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": false,
        "has_extended_profile": false,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 1,
      "favorite_count": 1,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "ja"
    },
    {
      "created_at": "Wed Jan 24 01:02:04 +0000 2018",
      "id": 955968911161225200,
      "id_str": "955968911161225216",
      "text": "Designer Marcelo Silva's (@marcelo14silva) fully-customizable smartwatch UI kit has everything you need to get star… https://t.co/grtZw00SZb",
      "truncated": true,
      "entities": {
        "hashtags": [],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "marcelo14silva",
            "name": "≛ Marcelo Silva",
            "id": 3253966298,
            "id_str": "3253966298",
            "indices": [
              26,
              41
            ]
          }
        ],
        "urls": [
          {
            "url": "https://t.co/grtZw00SZb",
            "expanded_url": "https://twitter.com/i/web/status/955968911161225216",
            "display_url": "twitter.com/i/web/status/9…",
            "indices": [
              117,
              140
            ]
          }
        ]
      },
      "source": "<a href=\"http://adobe.com\" rel=\"nofollow\">Adobe® Social</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 3689838915,
        "id_str": "3689838915",
        "name": "Adobe XD",
        "screen_name": "AdobeXD",
        "location": "",
        "description": "Exploring the design and prototyping of user experiences for websites and mobile apps. Talk with us! \n\nFormerly @AdobeUX",
        "url": "https://t.co/ReqqRXCT4c",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "https://t.co/ReqqRXCT4c",
                "expanded_url": "http://adobe.ly/xd",
                "display_url": "adobe.ly/xd",
                "indices": [
                  0,
                  23
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 33291,
        "friends_count": 134,
        "listed_count": 696,
        "created_at": "Thu Sep 17 21:54:59 +0000 2015",
        "favourites_count": 3492,
        "utc_offset": -28800,
        "time_zone": "Pacific Time (US & Canada)",
        "geo_enabled": false,
        "verified": true,
        "statuses_count": 6568,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "000000",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/920652173389656065/424EThdY_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/920652173389656065/424EThdY_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/3689838915/1512576119",
        "profile_link_color": "545454",
        "profile_sidebar_border_color": "000000",
        "profile_sidebar_fill_color": "000000",
        "profile_text_color": "000000",
        "profile_use_background_image": false,
        "has_extended_profile": false,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 7,
      "favorite_count": 27,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Wed Jan 24 01:00:40 +0000 2018",
      "id": 955968558349127700,
      "id_str": "955968558349127680",
      "text": "Illustration Teardowns—A look at shorthand techniques used to capture visual ideas quickly https://t.co/KIFdxp7el9… https://t.co/zElYSHOJN1",
      "truncated": true,
      "entities": {
        "hashtags": [],
        "symbols": [],
        "user_mentions": [],
        "urls": [
          {
            "url": "https://t.co/KIFdxp7el9",
            "expanded_url": "http://invs.io/2DF6Iau",
            "display_url": "invs.io/2DF6Iau",
            "indices": [
              91,
              114
            ]
          },
          {
            "url": "https://t.co/zElYSHOJN1",
            "expanded_url": "https://twitter.com/i/web/status/955968558349127680",
            "display_url": "twitter.com/i/web/status/9…",
            "indices": [
              116,
              139
            ]
          }
        ]
      },
      "source": "<a href=\"http://sproutsocial.com\" rel=\"nofollow\">Sprout Social</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 256093789,
        "id_str": "256093789",
        "name": "InVision",
        "screen_name": "InVisionApp",
        "location": "New York, NY",
        "description": "A Digital Product Design platform used to make the world’s \nbest customer experiences. Sign up free https://t.co/TVIUdSoBuA Need help? Hit up @invisionsupport",
        "url": "http://t.co/fRebRm4GXd",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/fRebRm4GXd",
                "expanded_url": "http://www.InVisionApp.com",
                "display_url": "InVisionApp.com",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": [
              {
                "url": "https://t.co/TVIUdSoBuA",
                "expanded_url": "http://invisionapp.com",
                "display_url": "invisionapp.com",
                "indices": [
                  100,
                  123
                ]
              }
            ]
          }
        },
        "protected": false,
        "followers_count": 119244,
        "friends_count": 5033,
        "listed_count": 2633,
        "created_at": "Tue Feb 22 17:02:10 +0000 2011",
        "favourites_count": 69117,
        "utc_offset": -18000,
        "time_zone": "Eastern Time (US & Canada)",
        "geo_enabled": true,
        "verified": true,
        "statuses_count": 43891,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/256093789/1509643240",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 4,
      "favorite_count": 7,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Wed Jan 24 01:00:03 +0000 2018",
      "id": 955968406158688300,
      "id_str": "955968406158688256",
      "text": "Learn how to create this retro text effect with @alex_vitori: https://t.co/Xdvk57PBZq #NationalHandwritingDay https://t.co/8bpclytTVR",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "NationalHandwritingDay",
            "indices": [
              86,
              109
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "alex_vitori",
            "name": "Alex (vitorials)",
            "id": 2403093217,
            "id_str": "2403093217",
            "indices": [
              48,
              60
            ]
          }
        ],
        "urls": [
          {
            "url": "https://t.co/Xdvk57PBZq",
            "expanded_url": "https://adobe.ly/2BnyGCD",
            "display_url": "adobe.ly/2BnyGCD",
            "indices": [
              62,
              85
            ]
          }
        ],
        "media": [
          {
            "id": 955968402509643800,
            "id_str": "955968402509643776",
            "indices": [
              110,
              133
            ],
            "media_url": "http://pbs.twimg.com/media/DURIMngVwAADZ2l.jpg",
            "media_url_https": "https://pbs.twimg.com/media/DURIMngVwAADZ2l.jpg",
            "url": "https://t.co/8bpclytTVR",
            "display_url": "pic.twitter.com/8bpclytTVR",
            "expanded_url": "https://twitter.com/Illustrator/status/955968406158688256/photo/1",
            "type": "photo",
            "sizes": {
              "thumb": {
                "w": 150,
                "h": 150,
                "resize": "crop"
              },
              "medium": {
                "w": 560,
                "h": 315,
                "resize": "fit"
              },
              "large": {
                "w": 560,
                "h": 315,
                "resize": "fit"
              },
              "small": {
                "w": 560,
                "h": 315,
                "resize": "fit"
              }
            }
          }
        ]
      },
      "extended_entities": {
        "media": [
          {
            "id": 955968402509643800,
            "id_str": "955968402509643776",
            "indices": [
              110,
              133
            ],
            "media_url": "http://pbs.twimg.com/media/DURIMngVwAADZ2l.jpg",
            "media_url_https": "https://pbs.twimg.com/media/DURIMngVwAADZ2l.jpg",
            "url": "https://t.co/8bpclytTVR",
            "display_url": "pic.twitter.com/8bpclytTVR",
            "expanded_url": "https://twitter.com/Illustrator/status/955968406158688256/photo/1",
            "type": "photo",
            "sizes": {
              "thumb": {
                "w": 150,
                "h": 150,
                "resize": "crop"
              },
              "medium": {
                "w": 560,
                "h": 315,
                "resize": "fit"
              },
              "large": {
                "w": 560,
                "h": 315,
                "resize": "fit"
              },
              "small": {
                "w": 560,
                "h": 315,
                "resize": "fit"
              }
            }
          }
        ]
      },
      "source": "<a href=\"http://adobe.com\" rel=\"nofollow\">Adobe® Social</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 23822236,
        "id_str": "23822236",
        "name": "Adobe Illustrator",
        "screen_name": "Illustrator",
        "location": "",
        "description": "Create logos, icons, sketches, typography and complex illustrations for print, web, interactive, video and mobile. Follow along for Illustrator tips & tricks.",
        "url": "https://t.co/2joJeTi45C",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "https://t.co/2joJeTi45C",
                "expanded_url": "http://adobe.ly/IllustratorCC",
                "display_url": "adobe.ly/IllustratorCC",
                "indices": [
                  0,
                  23
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 417841,
        "friends_count": 273,
        "listed_count": 4695,
        "created_at": "Wed Mar 11 20:22:34 +0000 2009",
        "favourites_count": 2658,
        "utc_offset": -28800,
        "time_zone": "Pacific Time (US & Canada)",
        "geo_enabled": false,
        "verified": true,
        "statuses_count": 15774,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "1A1B1F",
        "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/861988399/30137182f7dda0f22ee936b6f2cc0014.jpeg",
        "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/861988399/30137182f7dda0f22ee936b6f2cc0014.jpeg",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/879913584456642560/BpvwVn-2_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/879913584456642560/BpvwVn-2_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/23822236/1512497451",
        "profile_link_color": "E16614",
        "profile_sidebar_border_color": "FFFFFF",
        "profile_sidebar_fill_color": "E3E3E3",
        "profile_text_color": "303030",
        "profile_use_background_image": true,
        "has_extended_profile": true,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 27,
      "favorite_count": 123,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Wed Jan 24 00:00:43 +0000 2018",
      "id": 955953474470105100,
      "id_str": "955953474470105088",
      "text": "Take a look into the future of interactive games with @Nintendo's #NintendoLabo https://t.co/kNiZUcgd25 by… https://t.co/aSN2qlKePx",
      "truncated": true,
      "entities": {
        "hashtags": [
          {
            "text": "NintendoLabo",
            "indices": [
              66,
              79
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "Nintendo",
            "name": "任天堂株式会社",
            "id": 307902310,
            "id_str": "307902310",
            "indices": [
              54,
              63
            ]
          }
        ],
        "urls": [
          {
            "url": "https://t.co/kNiZUcgd25",
            "expanded_url": "http://invs.io/2DDFwsM",
            "display_url": "invs.io/2DDFwsM",
            "indices": [
              80,
              103
            ]
          },
          {
            "url": "https://t.co/aSN2qlKePx",
            "expanded_url": "https://twitter.com/i/web/status/955953474470105088",
            "display_url": "twitter.com/i/web/status/9…",
            "indices": [
              108,
              131
            ]
          }
        ]
      },
      "source": "<a href=\"http://sproutsocial.com\" rel=\"nofollow\">Sprout Social</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 256093789,
        "id_str": "256093789",
        "name": "InVision",
        "screen_name": "InVisionApp",
        "location": "New York, NY",
        "description": "A Digital Product Design platform used to make the world’s \nbest customer experiences. Sign up free https://t.co/TVIUdSoBuA Need help? Hit up @invisionsupport",
        "url": "http://t.co/fRebRm4GXd",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/fRebRm4GXd",
                "expanded_url": "http://www.InVisionApp.com",
                "display_url": "InVisionApp.com",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": [
              {
                "url": "https://t.co/TVIUdSoBuA",
                "expanded_url": "http://invisionapp.com",
                "display_url": "invisionapp.com",
                "indices": [
                  100,
                  123
                ]
              }
            ]
          }
        },
        "protected": false,
        "followers_count": 119244,
        "friends_count": 5033,
        "listed_count": 2633,
        "created_at": "Tue Feb 22 17:02:10 +0000 2011",
        "favourites_count": 69117,
        "utc_offset": -18000,
        "time_zone": "Eastern Time (US & Canada)",
        "geo_enabled": true,
        "verified": true,
        "statuses_count": 43891,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/256093789/1509643240",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 0,
      "favorite_count": 3,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Tue Jan 23 23:03:05 +0000 2018",
      "id": 955938967899140100,
      "id_str": "955938967899140096",
      "text": "Designer Michael Crabtree (@dAKirby309) uses fictional design briefs to create original prototypes using #AdobeXD o… https://t.co/93aPS3ndqm",
      "truncated": true,
      "entities": {
        "hashtags": [
          {
            "text": "AdobeXD",
            "indices": [
              105,
              113
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "dAKirby309",
            "name": "Michael",
            "id": 374878290,
            "id_str": "374878290",
            "indices": [
              27,
              38
            ]
          }
        ],
        "urls": [
          {
            "url": "https://t.co/93aPS3ndqm",
            "expanded_url": "https://twitter.com/i/web/status/955938967899140096",
            "display_url": "twitter.com/i/web/status/9…",
            "indices": [
              117,
              140
            ]
          }
        ]
      },
      "source": "<a href=\"http://adobe.com\" rel=\"nofollow\">Adobe® Social</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 3689838915,
        "id_str": "3689838915",
        "name": "Adobe XD",
        "screen_name": "AdobeXD",
        "location": "",
        "description": "Exploring the design and prototyping of user experiences for websites and mobile apps. Talk with us! \n\nFormerly @AdobeUX",
        "url": "https://t.co/ReqqRXCT4c",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "https://t.co/ReqqRXCT4c",
                "expanded_url": "http://adobe.ly/xd",
                "display_url": "adobe.ly/xd",
                "indices": [
                  0,
                  23
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 33291,
        "friends_count": 134,
        "listed_count": 696,
        "created_at": "Thu Sep 17 21:54:59 +0000 2015",
        "favourites_count": 3492,
        "utc_offset": -28800,
        "time_zone": "Pacific Time (US & Canada)",
        "geo_enabled": false,
        "verified": true,
        "statuses_count": 6568,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "000000",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/920652173389656065/424EThdY_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/920652173389656065/424EThdY_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/3689838915/1512576119",
        "profile_link_color": "545454",
        "profile_sidebar_border_color": "000000",
        "profile_sidebar_fill_color": "000000",
        "profile_text_color": "000000",
        "profile_use_background_image": false,
        "has_extended_profile": false,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 2,
      "favorite_count": 7,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Tue Jan 23 23:00:25 +0000 2018",
      "id": 955938299583098900,
      "id_str": "955938299583098880",
      "text": "\"I trained an #AI to copy my voice and it scared me silly.\" https://t.co/Ew5H1WsSDP via @TheNextWeb https://t.co/ZfoLaozRQK",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "AI",
            "indices": [
              14,
              17
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "TheNextWeb",
            "name": "TNW",
            "id": 10876852,
            "id_str": "10876852",
            "indices": [
              88,
              99
            ]
          }
        ],
        "urls": [
          {
            "url": "https://t.co/Ew5H1WsSDP",
            "expanded_url": "http://invs.io/2DqJPo0",
            "display_url": "invs.io/2DqJPo0",
            "indices": [
              60,
              83
            ]
          }
        ],
        "media": [
          {
            "id": 955938297750077400,
            "id_str": "955938297750077445",
            "indices": [
              100,
              123
            ],
            "media_url": "http://pbs.twimg.com/media/DUQs0SiVwAUNT42.jpg",
            "media_url_https": "https://pbs.twimg.com/media/DUQs0SiVwAUNT42.jpg",
            "url": "https://t.co/ZfoLaozRQK",
            "display_url": "pic.twitter.com/ZfoLaozRQK",
            "expanded_url": "https://twitter.com/InVisionApp/status/955938299583098880/photo/1",
            "type": "photo",
            "sizes": {
              "thumb": {
                "w": 150,
                "h": 150,
                "resize": "crop"
              },
              "medium": {
                "w": 796,
                "h": 398,
                "resize": "fit"
              },
              "large": {
                "w": 796,
                "h": 398,
                "resize": "fit"
              },
              "small": {
                "w": 680,
                "h": 340,
                "resize": "fit"
              }
            }
          }
        ]
      },
      "extended_entities": {
        "media": [
          {
            "id": 955938297750077400,
            "id_str": "955938297750077445",
            "indices": [
              100,
              123
            ],
            "media_url": "http://pbs.twimg.com/media/DUQs0SiVwAUNT42.jpg",
            "media_url_https": "https://pbs.twimg.com/media/DUQs0SiVwAUNT42.jpg",
            "url": "https://t.co/ZfoLaozRQK",
            "display_url": "pic.twitter.com/ZfoLaozRQK",
            "expanded_url": "https://twitter.com/InVisionApp/status/955938299583098880/photo/1",
            "type": "photo",
            "sizes": {
              "thumb": {
                "w": 150,
                "h": 150,
                "resize": "crop"
              },
              "medium": {
                "w": 796,
                "h": 398,
                "resize": "fit"
              },
              "large": {
                "w": 796,
                "h": 398,
                "resize": "fit"
              },
              "small": {
                "w": 680,
                "h": 340,
                "resize": "fit"
              }
            }
          }
        ]
      },
      "source": "<a href=\"http://sproutsocial.com\" rel=\"nofollow\">Sprout Social</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 256093789,
        "id_str": "256093789",
        "name": "InVision",
        "screen_name": "InVisionApp",
        "location": "New York, NY",
        "description": "A Digital Product Design platform used to make the world’s \nbest customer experiences. Sign up free https://t.co/TVIUdSoBuA Need help? Hit up @invisionsupport",
        "url": "http://t.co/fRebRm4GXd",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/fRebRm4GXd",
                "expanded_url": "http://www.InVisionApp.com",
                "display_url": "InVisionApp.com",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": [
              {
                "url": "https://t.co/TVIUdSoBuA",
                "expanded_url": "http://invisionapp.com",
                "display_url": "invisionapp.com",
                "indices": [
                  100,
                  123
                ]
              }
            ]
          }
        },
        "protected": false,
        "followers_count": 119244,
        "friends_count": 5033,
        "listed_count": 2633,
        "created_at": "Tue Feb 22 17:02:10 +0000 2011",
        "favourites_count": 69117,
        "utc_offset": -18000,
        "time_zone": "Eastern Time (US & Canada)",
        "geo_enabled": true,
        "verified": true,
        "statuses_count": 43891,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/256093789/1509643240",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 4,
      "favorite_count": 9,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Tue Jan 23 22:54:44 +0000 2018",
      "id": 955936868318691300,
      "id_str": "955936868318691328",
      "text": "RT @uxdesigncc: Evil UX, designers and words, guide to UX research and more links this week https://t.co/0hpVXOIjHE",
      "truncated": false,
      "entities": {
        "hashtags": [],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "uxdesigncc",
            "name": "UX Design Collective",
            "id": 16814473,
            "id_str": "16814473",
            "indices": [
              3,
              14
            ]
          }
        ],
        "urls": [
          {
            "url": "https://t.co/0hpVXOIjHE",
            "expanded_url": "https://buff.ly/2mW0Jnj",
            "display_url": "buff.ly/2mW0Jnj",
            "indices": [
              92,
              115
            ]
          }
        ]
      },
      "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 256093789,
        "id_str": "256093789",
        "name": "InVision",
        "screen_name": "InVisionApp",
        "location": "New York, NY",
        "description": "A Digital Product Design platform used to make the world’s \nbest customer experiences. Sign up free https://t.co/TVIUdSoBuA Need help? Hit up @invisionsupport",
        "url": "http://t.co/fRebRm4GXd",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/fRebRm4GXd",
                "expanded_url": "http://www.InVisionApp.com",
                "display_url": "InVisionApp.com",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": [
              {
                "url": "https://t.co/TVIUdSoBuA",
                "expanded_url": "http://invisionapp.com",
                "display_url": "invisionapp.com",
                "indices": [
                  100,
                  123
                ]
              }
            ]
          }
        },
        "protected": false,
        "followers_count": 119244,
        "friends_count": 5033,
        "listed_count": 2633,
        "created_at": "Tue Feb 22 17:02:10 +0000 2011",
        "favourites_count": 69117,
        "utc_offset": -18000,
        "time_zone": "Eastern Time (US & Canada)",
        "geo_enabled": true,
        "verified": true,
        "statuses_count": 43891,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/593893225045200896/r9uL4jWU_normal.png",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/256093789/1509643240",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "retweeted_status": {
        "created_at": "Mon Jan 22 19:03:05 +0000 2018",
        "id": 955516183087894500,
        "id_str": "955516183087894529",
        "text": "Evil UX, designers and words, guide to UX research and more links this week https://t.co/0hpVXOIjHE",
        "truncated": false,
        "entities": {
          "hashtags": [],
          "symbols": [],
          "user_mentions": [],
          "urls": [
            {
              "url": "https://t.co/0hpVXOIjHE",
              "expanded_url": "https://buff.ly/2mW0Jnj",
              "display_url": "buff.ly/2mW0Jnj",
              "indices": [
                76,
                99
              ]
            }
          ]
        },
        "source": "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
          "id": 16814473,
          "id_str": "16814473",
          "name": "UX Design Collective",
          "screen_name": "uxdesigncc",
          "location": "San Francisco, CA",
          "description": "Curated stories on user experience, usability, and product design. By @fabriciot and @caioab.",
          "url": "https://t.co/MtolQrfsUL",
          "entities": {
            "url": {
              "urls": [
                {
                  "url": "https://t.co/MtolQrfsUL",
                  "expanded_url": "http://uxdesign.cc",
                  "display_url": "uxdesign.cc",
                  "indices": [
                    0,
                    23
                  ]
                }
              ]
            },
            "description": {
              "urls": []
            }
          },
          "protected": false,
          "followers_count": 16246,
          "friends_count": 14,
          "listed_count": 447,
          "created_at": "Thu Oct 16 21:19:49 +0000 2008",
          "favourites_count": 137,
          "utc_offset": -18000,
          "time_zone": "Eastern Time (US & Canada)",
          "geo_enabled": true,
          "verified": false,
          "statuses_count": 8705,
          "lang": "en",
          "contributors_enabled": false,
          "is_translator": false,
          "is_translation_enabled": false,
          "profile_background_color": "FFFFFF",
          "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/303719987/cropped-polarbear-098.jpeg",
          "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/303719987/cropped-polarbear-098.jpeg",
          "profile_background_tile": true,
          "profile_image_url": "http://pbs.twimg.com/profile_images/378800000018073792/6fd28fe6993780bbc1a9fc733fa26c15_normal.png",
          "profile_image_url_https": "https://pbs.twimg.com/profile_images/378800000018073792/6fd28fe6993780bbc1a9fc733fa26c15_normal.png",
          "profile_link_color": "00CCFF",
          "profile_sidebar_border_color": "FFFFFF",
          "profile_sidebar_fill_color": "EFEFEF",
          "profile_text_color": "333333",
          "profile_use_background_image": false,
          "has_extended_profile": false,
          "default_profile": false,
          "default_profile_image": false,
          "following": false,
          "follow_request_sent": false,
          "notifications": false,
          "translator_type": "none"
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "is_quote_status": false,
        "retweet_count": 5,
        "favorite_count": 17,
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
      },
      "is_quote_status": false,
      "retweet_count": 5,
      "favorite_count": 0,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    }
  ],
  lists: [
    {
      "id": 956157740585332700,
      "id_str": "956157740585332736",
      "name": "Music",
      "uri": "/hey_degital/lists/music",
      "subscriber_count": 0,
      "member_count": 7,
      "mode": "public",
      "description": "",
      "slug": "music",
      "full_name": "@hey_degital/music",
      "created_at": "Wed Jan 24 13:32:24 +0000 2018",
      "following": true,
      "user": {
        "id": 775003201338683400,
        "id_str": "775003201338683393",
        "name": "へい",
        "screen_name": "hey_degital",
        "location": "",
        "description": "",
        "url": null,
        "entities": {
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 9,
        "friends_count": 14,
        "listed_count": 1,
        "created_at": "Sun Sep 11 16:08:56 +0000 2016",
        "favourites_count": 50,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 106,
        "lang": "ja",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "F5F8FA",
        "profile_background_image_url": null,
        "profile_background_image_url_https": null,
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/775004516211011584/65QmiiDz_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/775004516211011584/65QmiiDz_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/775003201338683393/1473610539",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      }
    },
    {
      "id": 956154728735322100,
      "id_str": "956154728735322112",
      "name": "DesignTool",
      "uri": "/hey_degital/lists/designtool",
      "subscriber_count": 0,
      "member_count": 9,
      "mode": "public",
      "description": "tool",
      "slug": "designtool",
      "full_name": "@hey_degital/designtool",
      "created_at": "Wed Jan 24 13:20:26 +0000 2018",
      "following": true,
      "user": {
        "id": 775003201338683400,
        "id_str": "775003201338683393",
        "name": "へい",
        "screen_name": "hey_degital",
        "location": "",
        "description": "",
        "url": null,
        "entities": {
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 9,
        "friends_count": 14,
        "listed_count": 1,
        "created_at": "Sun Sep 11 16:08:56 +0000 2016",
        "favourites_count": 50,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 106,
        "lang": "ja",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "F5F8FA",
        "profile_background_image_url": null,
        "profile_background_image_url_https": null,
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/775004516211011584/65QmiiDz_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/775004516211011584/65QmiiDz_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/775003201338683393/1473610539",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      }
    },
    {
      "id": 956149708145045500,
      "id_str": "956149708145045505",
      "name": "Designer",
      "uri": "/hey_degital/lists/designer1",
      "subscriber_count": 0,
      "member_count": 12,
      "mode": "public",
      "description": "designer",
      "slug": "designer1",
      "full_name": "@hey_degital/designer1",
      "created_at": "Wed Jan 24 13:00:29 +0000 2018",
      "following": true,
      "user": {
        "id": 775003201338683400,
        "id_str": "775003201338683393",
        "name": "へい",
        "screen_name": "hey_degital",
        "location": "",
        "description": "",
        "url": null,
        "entities": {
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 9,
        "friends_count": 14,
        "listed_count": 1,
        "created_at": "Sun Sep 11 16:08:56 +0000 2016",
        "favourites_count": 50,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 106,
        "lang": "ja",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "F5F8FA",
        "profile_background_image_url": null,
        "profile_background_image_url_https": null,
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/775004516211011584/65QmiiDz_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/775004516211011584/65QmiiDz_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/775003201338683393/1473610539",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      }
    },
    {
      "id": 949477432762888200,
      "id_str": "949477432762888192",
      "name": "AWS",
      "uri": "/hey_degital/lists/aws",
      "subscriber_count": 0,
      "member_count": 4,
      "mode": "public",
      "description": "AWS関連リスト",
      "slug": "aws",
      "full_name": "@hey_degital/aws",
      "created_at": "Sat Jan 06 03:07:15 +0000 2018",
      "following": true,
      "user": {
        "id": 775003201338683400,
        "id_str": "775003201338683393",
        "name": "へい",
        "screen_name": "hey_degital",
        "location": "",
        "description": "",
        "url": null,
        "entities": {
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 9,
        "friends_count": 14,
        "listed_count": 1,
        "created_at": "Sun Sep 11 16:08:56 +0000 2016",
        "favourites_count": 50,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 106,
        "lang": "ja",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "F5F8FA",
        "profile_background_image_url": null,
        "profile_background_image_url_https": null,
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/775004516211011584/65QmiiDz_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/775004516211011584/65QmiiDz_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/775003201338683393/1473610539",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      }
    }
  ]
};
