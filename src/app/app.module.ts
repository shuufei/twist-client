import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativeStorage } from '@ionic-native/native-storage';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { TimelinePage } from '../pages/timeline/timeline';
import { ComponentsModule } from '../components/components.module';
import { FilterPopupComponent } from '../components/filter-popup/filter-popup';
import { DataProvider } from '../providers/data/data';
import { ListProvider } from '../providers/list/list';
import { TwitterClientProvider } from '../providers/twitter-client/twitter-client';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { FilterProvider } from '../providers/filter/filter';

@NgModule({
  declarations: [
    MyApp,
    TimelinePage,
    FilterPopupComponent
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TimelinePage,
    FilterPopupComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativeStorage,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TwitterClientProvider,
    DataProvider,
    ListProvider,
    InAppBrowser,
    FilterProvider
  ]
})
export class AppModule {}
