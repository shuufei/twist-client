import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the MediaPopupComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'media-popup',
  templateUrl: 'media-popup.html'
})
export class MediaPopupComponent {
  @Input() url: string;
  @Input() type: string;
  @Output() close = new EventEmitter<void>();

  constructor() {}

}
