import { Component, Output, EventEmitter } from '@angular/core';
import { ListProvider } from '../../providers/list/list';

/**
 * Generated class for the TripperHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  selector: 'tripper-header',
  templateUrl: 'tripper-header.html'
})
export class TripperHeaderComponent {
  @Output() slide = new EventEmitter();
  myLists = [];
  activeList;

  constructor(
    private listService: ListProvider
  ) {
    this.listService.listsState.subscribe((lists) => {
      this.listService.setActiveList(lists[0]);
      this.myLists = lists;
    });
    this.listService.activeListState.subscribe((list) => {
      this.activeList = list;
    });
  }

  changeActiveList(list) {
    // this.listService.setActiveList(list);
    this.slide.emit(list);
  }
}
