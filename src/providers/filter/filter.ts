import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

// providers
import { ListProvider } from '../list/list';

/*
  Generated class for the FilterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FilterProvider {

  public readonly FILTER_ITEM = {
    ALL: 'all',
    FAVORITES: 'favorites'
  };

  public activeFilters: Array<any> = [];

  public activeFilterSubject = new Subject<any>();
  public activeFilterState = this.activeFilterSubject.asObservable();

  constructor(private listService: ListProvider) {
  }

  setInitialFilters(filters: Array<any>) {
    this.activeFilters = filters;
  }

  changeFilter(filter: string) {
    let activeList = this.listService.getActiveList();
    if (this.activeFilters[activeList.index] !== filter) {
      this.activeFilters[activeList.index] = filter;
      this.activeFilterSubject.next(this.activeFilters);
    }
  }
}
