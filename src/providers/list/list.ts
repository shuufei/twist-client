import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

/*
  Generated class for the ListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ListProvider {
  // TODO: specify list object.
  private activeList: any;
  private lists: Array<any>;
  private listTweets: Array<any> = [];

  // TODO: specify list object.
  public activeListSubject = new Subject<any>();
  public activeListState = this.activeListSubject.asObservable();

  // TODO: specify list object.
  public listsSubject = new Subject<any>();
  public listsState = this.listsSubject.asObservable();

  // TODO: specify list object.
  public listTweetsSubject = new Subject<any>();
  public listTweetsState = this.listTweetsSubject.asObservable();

  constructor() {}

  setActiveList(list): void {
    this.activeList = list;
    this.activeListSubject.next(list);
  }

  setLists(lists): void {
    this.lists = lists;
    this.setInitialListTweets();
    this.listsSubject.next(lists);
  }

  setInitialListTweets() {
    this.lists.map((list) => {
      this.listTweets.push({
        listId: list.id_str,
        listName: list.name,
        tweets: []
      });
    });
    this.listTweetsSubject.next(this.listTweets);
  }

  setListTweets(index, tweets, latest) {
    if (latest) {
      this.listTweets[index].tweets = tweets.concat(this.listTweets[index].tweets);
    } else {
      this.listTweets[index].tweets = this.listTweets[index].tweets.concat(tweets);
    }
    this.listTweetsSubject.next(this.listTweets);
  }

  resetListTweets(index, tweets) {
    this.listTweets[index].tweets = tweets;
    this.listTweetsSubject.next(this.listTweets);
  }

  getActiveList() {
    return this.activeList;
  }

  getLists() {
    return this.lists;
  }

  getListTweets() {
    return this.listTweets;
  }

  isInitializedListTweets() {
    return this.lists.length === this.listTweets.length;
  }

}
