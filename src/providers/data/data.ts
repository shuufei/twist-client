import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { NativeStorage } from '@ionic-native/native-storage';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {

  constructor(
    public platform: Platform,
    public storage: Storage,
    private nativeStorage: NativeStorage
  ) {
    console.log('Hello DataProvider Provider');
  }

  isCordova() {
    return this.platform.is('cordova');
  }

  // TODO: エラーちゃんと返す
  getData(key) {
    console.log('=== get data in data provider ===', key);
    if (this.isCordova()) {
      return this.nativeStorage.getItem(key)
        .then((res) => {
          console.log('=== getData by nativeStorage in DataProvider ===');
          console.log(res);
          return res;
        })
        .catch((err) => {
          console.log('=== err getData by nativeStorage in DataProvider ===');
          console.log(err);
        });
    } else {
      return this.storage.get(key)
        .then((res) => {
          console.log('=== getData by storage in DataProvider ===');
          console.log(res);
          return res;
        })
        .catch((err) => {
          console.log('=== err getData by storage in DataProvider ===');
          console.log(err);
        });
    }
  }

  setData(key, data) {
    console.log('=== set data in data provider ===');
    if (this.isCordova()) {
      this.nativeStorage.setItem(key, data)
        .then((res) => {
          console.log('=== setData by nativeStorage in DataProvider ===');
          console.log(res);
          return res;
        })
        .catch((err) => {
          console.log('=== err getData by nativeStorage in DataProvider ===');
          console.log(err);
        });
    } else {
      return this.storage.set(key, data)
        .then((res) => {
          console.log('=== setData by storage in DataProvider ===');
          console.log(res);
          return res;
        })
        .catch((err) => {
          console.log('=== err setData by storage in DataProvider ===');
          console.log(err);
        });
    }
  }

  removeData(key) {
    console.log('=== remove data in data provider ===');
    if (this.isCordova()) {
      return this.nativeStorage.remove(key)
        .then((res) => {
          console.log('=== removeData by nativeStorage in DataProvider ===');
          console.log(res);
          return res;
        })
        .catch((err) => {
          console.log('=== err getDremoveDataata by nativeStorage in DataProvider ===');
          console.log(err);
        });
    } else {
      return this.storage.remove(key)
        .then((res) => {
          console.log('=== removeData by storage in DataProvider ===');
          console.log(res);
          return res;
        })
        .catch((err) => {
          console.log('=== err removeData by storage in DataProvider ===');
          console.log(err);
        });
    }
  }

  clearData() {
    if (this.isCordova()) {
      return this.nativeStorage.clear()
        .then((res) => {
          console.log('=== clear by nativeStorage ===');
          console.log(res);
          return res;
        })
        .catch((err) => {
          console.log('=== clear failed by nativeStorage ===');
          console.log(err);
        });
    } else {
      return this.storage.clear()
        .then((res) => {
          console.log('=== clear by storage ===');
          console.log(res);
          return res;
        })
        .catch((err) => {
          console.log('=== clear failed by storage ===');
          console.log(err);
        });
    }
  }
}
