import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, Platform } from 'ionic-angular';

// provider
import { TwitterClientProvider } from '../../providers/twitter-client/twitter-client';
import { DataProvider } from '../../providers/data/data';
import { ListProvider } from '../../providers/list/list';
import { FilterProvider } from '../../providers/filter/filter';

/**
 * Generated class for the TimelinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

const MY_LIST = 'myList';
const NEXT_TRIGGER_FOR_WEB = 0;
const NEXT_TRIGGER_FOR_CORDOVA = -30;
const OLD_SCROLL_BUFFER = 10;
const TWEET_COUNT = 50;
const RT_FLG = false;

@Component({
  selector: 'page-timeline',
  templateUrl: 'timeline.html',
})
export class TimelinePage {
  @ViewChild(Slides) slides: Slides;
  private myLists = [];
  private timelines: any;
  private smoothScroll = true;
  private isGettingNextTweet = [];
  private isGettingOldTweet = [];
  private nextLoadingSpin = [];
  private oldTweetGettingThreshold = [];
  private isShowExpandMedia: boolean = false;
  private mediaUrl: string = '';
  private mediaType: string = '';
  private activeFilters: Array<string> = [];
  private initialFilter:string = 'all';
  private notFoundTweets: Array<any> = [];
  private isOpenFilter: boolean = false;
  private isInitialized: Array<boolean> = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private twitterClient: TwitterClientProvider,
    private data: DataProvider,
    private listService: ListProvider,
    private platform: Platform,
    private filterService: FilterProvider
  ) {}

  ionViewDidLoad() {
    this.listService.listsState.subscribe((lists) => {
      this.myLists = lists;
      // initialieze get tweet status.
      this.myLists.map((list) => {
        this.isGettingNextTweet.push(false);
        this.isGettingOldTweet.push(false);
        this.nextLoadingSpin.push(false);
        this.oldTweetGettingThreshold.push(false);
        this.activeFilters.push(this.initialFilter);
        this.notFoundTweets.push(false);
        this.isInitialized.push(false);
      });
      this.filterService.setInitialFilters(this.activeFilters);
      // 描画が追いついていないのでまつ
      setTimeout(() => {
        this.timelines = document.getElementsByTagName('ion-slide');
        for (let i = 0; i < this.timelines.length; i++) {
          this.timelines[i].addEventListener('scroll', (event) => {
            let activeListIndex = this.listService.getActiveList().index;
            let triggerThreshold = this.platform.is('cordova') ? NEXT_TRIGGER_FOR_CORDOVA : NEXT_TRIGGER_FOR_WEB;
            let scrollTop = event.srcElement.scrollTop;
            if (!this.isGettingNextTweet[activeListIndex] && scrollTop <= triggerThreshold) {
              this.isGettingNextTweet[activeListIndex] = true;
              this.nextLoadingSpin[activeListIndex] = true;
              this.getNextTweet(activeListIndex);
            }
            if (!this.oldTweetGettingThreshold[activeListIndex]) {
              this.oldTweetGettingThreshold[activeListIndex] = event.srcElement.scrollHeight - window.innerHeight - OLD_SCROLL_BUFFER;
            }
            if (!this.isGettingOldTweet[activeListIndex] && this.oldTweetGettingThreshold[activeListIndex] < scrollTop) {
              this.isGettingOldTweet[activeListIndex] = true;
              this.getOldTweet(activeListIndex);
            }
          });
        }
      }, 500);
    });

    this.listService.activeListState.subscribe((list) => {
      if (this.listService.isInitializedListTweets() &&
            !this.isInitialized[list.index]) {
      // if (this.listService.isInitializedListTweets() &&
      //       this.listService.getListTweets()[list.index].tweets.length === 0) {
        this.getTimelineTweets(list.id_str, list.index)
          .then(() => {
            this.isInitialized[list.index] = true;
          });
      }
    });

    this.filterService.activeFilterState.subscribe((filters) => {
      console.log('=== change filter from timeline ctrl ===');
      let list = this.listService.getActiveList();
      this.activeFilters = filters;

      this.refreshTimeline(list.index);

      switch(this.activeFilters[list.index]) {
        case this.filterService.FILTER_ITEM.ALL:
          this.twitterClient.getStatuses(list.id_str, null, null, TWEET_COUNT, RT_FLG)
            .then((res) => {
              this.listService.resetListTweets(list.index, res);
            });
          break;
        case this.filterService.FILTER_ITEM.FAVORITES:
          this.twitterClient.getFavoStatuses(list.id_str, null)
            .then((res: Array<any>) => {
              if (res.length === 0) {
                this.notFoundTweets[list.index] = true;
              }
              this.listService.resetListTweets(list.index, res);
            });
          break;
      }
    });
    // すぐにnative storageから取得してしまうとうまく取得できずに、リスト取得リクエストが実行されてしまう。
    setTimeout(() => {
      this.data.getData(MY_LIST)
        .then((data) => {
          if (!data) {
            console.log('=== lists is not exist ===');
            this.initializeLists();
          } else {
            console.log('=== lists is exist ===');
            this.listService.setLists(data);
          }
        })
        .catch((err) => {
          console.log('=== get lists failed or lists is not exist===', err);
          this.initializeLists();
        });
    }, 1000);
  }

  refreshTimeline(listIndex) {
    this.notFoundTweets[listIndex] = false;
    this.listService.resetListTweets(listIndex, []);
    this.oldTweetGettingThreshold[listIndex] = false;
  }

  getTimelineTweets(listId, listIndex): Promise<any> {
    return this.twitterClient.getStatuses(listId, null, null, TWEET_COUNT, RT_FLG)
      .then((res: Array<any>) => {
        console.log('=== first view ===', res);
        let tweets: Array<any> = res;
        this.listService.setListTweets(listIndex, tweets, true);
        return;
      });
  }

  initializeLists() {
    this.twitterClient.getLists(null)
      .then((lists: Array<any>) => {
        lists.forEach((list, index) => {
          list.index = index;
        });
        this.data.setData(MY_LIST, lists);
        this.listService.setLists(lists);
      })
      .catch((err) => {
        console.log('=== getlists error in timeline ===', err);
      });
  }

  slideChange() {
    let activeIndex = this.slides.getActiveIndex();
    activeIndex = activeIndex === this.myLists.length ? --activeIndex : activeIndex;
    if (activeIndex !== this.listService.getActiveList().index) {
      this.listService.setActiveList(this.myLists[activeIndex]);
    }
    // this.filterService.setFilter('all');
  }

  slideTo(list) {
    if (this.listService.getActiveList().index === list.index) {
      let duration = this.timelines[list.index].scrollTop <= 300 ? 30 : 300;
      this.scrollTo(this.timelines[list.index], 0, duration);
      return;
    }
    this.listService.setActiveList(list);
    this.slides.slideTo(list.index);
    // this.filterService.setFilter('all');
  }

  scrollTop() {
    let list = this.listService.getActiveList();
    let duration = this.timelines[list.index].scrollTop <= 300 ? 30 : 300;
    this.scrollTo(this.timelines[list.index], 0, duration);
  }

  scrollBottom() {
    let list = this.listService.getActiveList();
    let duration = 300;
    let bottom = this.timelines[list.index].scrollHeight - window.innerHeight;
    if (!this.oldTweetGettingThreshold[list.index]) {
      this.oldTweetGettingThreshold[list.index] = bottom - OLD_SCROLL_BUFFER;
    }
    this.isGettingOldTweet[list.index] = true;
    this.getOldTweet(list.index);
    this.scrollTo(this.timelines[list.index], bottom, duration);
  }

  // TODO: util provider.
  scrollTo(element, to, duration) {
    this.smoothScroll = false;
    if (duration <= 0) {
      this.smoothScroll = true;
      return;
    };
    var difference = to - element.scrollTop;
    var perTick = difference / duration * 10;

    setTimeout(() => {
        element.scrollTop = element.scrollTop + perTick;
        if (element.scrollTop === to) {
          this.smoothScroll = true;
          return;
        };
        this.scrollTo(element, to, duration - 10);
    }, 10);
  }

  // TODO: 20件以上あった時の対応refactor
  async getNextTweet(activeListIndex) {
    let list = this.listService.getLists()[activeListIndex];
    let activeListTweets = this.listService.getListTweets()[activeListIndex].tweets;
    if (activeListTweets.length === 0) {
      this.nextLoadingSpin[activeListIndex] = false;
      this.isGettingNextTweet[activeListIndex] = false;
      return;
    }
    let latestTweet = activeListTweets[0];
    let nextTweets = [];
    let maxId = null;
    let tweetLength: number;
    do {
      if (nextTweets.length > 0) {
        maxId = nextTweets[nextTweets.length-1].id_str;
      }
      let tweets: any = await this.twitterClient.getStatuses(list.id_str, latestTweet.id_str, maxId, TWEET_COUNT, RT_FLG);
      tweetLength = tweets.length;
      if (tweetLength !== 0) {
        // 末尾のtweetがlatestTweetだった場合、除去
        if (tweets[tweets.length-1].id_str === latestTweet.id_str) {
          tweets.pop();
        }
        // 先頭が取得済みだった場合、除去 TODO: refactor
        if (maxId !== null && tweets.length > 0 && tweets[0].id_str === nextTweets[nextTweets.length-1].id_str) {
          tweets.shift();
        }
        // maxIdのtweetを除去してlist長が0だともうtweetがない
        if (tweets.length === 0) {
          tweetLength = 0;
        } else {
          nextTweets = nextTweets.concat(tweets);
        }
      }
    } while (tweetLength !== 0);
    setTimeout(() => {
      if (this.activeFilters[activeListIndex] === 'favorites') {
        nextTweets = nextTweets.filter((tweet) => {
          return tweet.favorited;
        });
      }
      this.listService.setListTweets(activeListIndex, nextTweets, true);
      this.nextLoadingSpin[activeListIndex] = false;
      this.isGettingNextTweet[activeListIndex] = false;
    }, 500);
  }

  getOldTweet(activeListIndex) {
    console.log('=== getOldTweet ===');
    let list = this.listService.getLists()[activeListIndex];
    let activeListTweets = this.listService.getListTweets()[activeListIndex].tweets;
    if (activeListTweets.length === 0) {
      this.isGettingOldTweet[activeListIndex] = false;
      this.oldTweetGettingThreshold[activeListIndex] = false;
      return;
    }
    let oldestTweet = activeListTweets[activeListTweets.length-1];
    if (this.activeFilters[list.index] === 'favorites') {
      this.twitterClient.getFavoStatuses(list.id_str, oldestTweet.id_str)
        .then((tweets: Array<any>) => {
          if (tweets.length > 0 && tweets[0].id_str === oldestTweet.id_str) {
            tweets.shift();
          }
          if (tweets.length === 0) {
            this.notFoundTweets[list.index] = true;
          }
          this.listService.setListTweets(activeListIndex, tweets, false);
          this.isGettingOldTweet[activeListIndex] = false;
          this.oldTweetGettingThreshold[activeListIndex] = false;
        })
        .catch((err) => {
          console.log('=== error ===', err);
        });
    } else {
      this.twitterClient.getStatuses(list.id_str, null, oldestTweet.id_str, TWEET_COUNT, RT_FLG)
        .then((tweets: Array<any>) => {
          if (tweets.length > 0 && tweets[0].id_str === oldestTweet.id_str) {
            tweets.shift();
          }
          this.listService.setListTweets(activeListIndex, tweets, false);
          this.isGettingOldTweet[activeListIndex] = false;
          this.oldTweetGettingThreshold[activeListIndex] = false;
        });
    }
  }

  expandMedia(media) {
    console.log('=== expand image ===', media);
    if (media) {
      this.isShowExpandMedia = true;
      this.mediaUrl = media.media_url;
      this.mediaType = media.type;
      document.addEventListener( 'touchmove', this.scrollOff, false);
    }
  }

  closeExpandMedia() {
    document.removeEventListener( 'touchmove', this.scrollOff, false );
    this.isShowExpandMedia = false;
  }

  scrollOff(e) {
    e.preventDefault();
  }

  openFilterPopup() {
    this.isOpenFilter = true;
  }
  closeFilterPopup() {
    this.isOpenFilter = false;
  }

  changeFilter(filter: string) {
    this.filterService.changeFilter(filter);
    this.closeFilterPopup();
  }

  // debug
  removeLists() {
    this.data.removeData(MY_LIST)
      .then((res) => {
        console.log('=== remove result ===');
      });
  }

  // debug
  getData() {
    this.data.getData(MY_LIST)
      .then((data) => {
        console.log('=== getData ===', data);
      })
      .catch((err) => {
        console.log('=== promise catch getData ===', err);
      });
  }
}
