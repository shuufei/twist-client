import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import twitterText from 'twitter-text';

// provider
import { TwitterClientProvider } from '../../providers/twitter-client/twitter-client';

/**
 * Generated class for the TweetComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  selector: 'tweet',
  templateUrl: 'tweet.html'
})
export class TweetComponent {
  @Input() tweet;
  @Output() expandMedia = new EventEmitter<any>();

  private tweetTextList: Array<any> = [];
  private mediaList: Array<any> = [];
  private TWITTER_ENDPOINT = 'https://twitter.com';
  private TEXT_TYPE = {
    TEXT: 'text',
    HASHTAG: 'hashtag',
    URL: 'url',
    USER_MENTION: 'userMention',
    MEDIA: 'media'
  };

  constructor(
    private theInAppBrowser: InAppBrowser,
    private platform: Platform,
    private sanitization: DomSanitizer,
    private twitterClient: TwitterClientProvider
  ) {}

  ngOnInit() {
    this.setOriginalUserImg();
    this.initializeTweet(this.tweet);
  }

  setOriginalUserImg() {
    this.tweet.user.profile_image_url = this.tweet.user.profile_image_url.replace(/_normal/g, '');
  }

  initializeTweet(tweet) {
    this.convertUnicodeIndices(tweet);
    let entitiesList: Array<any> = [];
    if (tweet.entities.urls.length > 0) {
      tweet.entities.urls.map((url) => {
        entitiesList.push({
          startIndex: url.indices[0],
          endIndex: url.indices[1],
          text: url.url,
          type: this.TEXT_TYPE.URL
        });
      });
    }
    if (tweet.entities.hashtags.length > 0) {
      tweet.entities.hashtags.map((hashtag) => {
        entitiesList.push({
          startIndex: hashtag.indices[0],
          endIndex: hashtag.indices[1],
          text: `${hashtag.text}`,
          type: this.TEXT_TYPE.HASHTAG
        });
      });
    }
    if (tweet.entities.user_mentions.length > 0) {
      tweet.entities.user_mentions.map((userMention) => {
        entitiesList.push({
          startIndex: userMention.indices[0],
          endIndex: userMention.indices[1],
          text: `${userMention.screen_name}`,
          type: this.TEXT_TYPE.USER_MENTION
        });
      });
    }
    if (tweet.entities.media && tweet.entities.media.length > 0) {
      tweet.entities.media.map((media) => {
        entitiesList.push({
          startIndex: media.indices[0],
          endIndex: media.indices[1],
          text: media.url,
          mediaUrl: media.media_url,
          type: this.TEXT_TYPE.MEDIA
        });
      });
      if (tweet.extended_entities) {
        tweet.extended_entities.media.map((media) => {
          // console.log('=== media url ===', media);
          this.mediaList.push(media);
        })
      }
    }

    entitiesList = entitiesList.sort(this.compare);
    this.createTweetTextList(tweet.text, entitiesList);
  }

  compare(a, b) {
    let indexA = a.startIndex;
    let indexB = b.startIndex;

    let comparison = 0;
    if (indexA > indexB) {
      comparison = 1;
    } else if (indexA < indexB) {
      comparison = -1;
    }
    return comparison;
  }

  convertUnicodeIndices(tweet) {
    twitterText.convertUnicodeIndices(tweet.text, tweet.entities.urls);
    twitterText.convertUnicodeIndices(tweet.text, tweet.entities.hashtags);
    twitterText.convertUnicodeIndices(tweet.text, tweet.entities.user_mentions);
    if (tweet.entities.media) {
      twitterText.convertUnicodeIndices(tweet.text, tweet.entities.media);
    }
  }

  createTweetTextList(text, entitiesList) {
    let textStartIndex = 0;
    if (entitiesList.length === 0) {
      // console.log('=== entities list ===', entitiesList);
      this.tweetTextList.push({
        startIndex: textStartIndex,
        endIndex: -1,
        text: text,
        type: this.TEXT_TYPE.TEXT
      });
    }
    entitiesList.map((entities) => {
      // entitiesのstartIndexが0の場合は画像などで始まってるのでtextは不要
      if (entities.startIndex !== 0) {
        this.tweetTextList.push({
          startIndex: textStartIndex,
          endIndex: entities.startIndex - 1,
          text: text.slice(textStartIndex, entities.startIndex - 1),
          type: this.TEXT_TYPE.TEXT
        });
      }
      this.tweetTextList.push(entities);
      textStartIndex = entities.endIndex + 1;
    });
    // console.log('=== tweetTextList ===');
    // console.log(this.tweetTextList);
  }

  openBrowser(url) {
    if (this.platform.is('cordova')) {
      this.theInAppBrowser.create(url, '_system');
    }　else {
      window.open(url, '_blank');
    }
  }

  openHashtag(hashtag) {
    let url = `${this.TWITTER_ENDPOINT}/hashtag/${hashtag}?src=hash`;
    this.openBrowser(url);
  }

  openUser(user) {
    let url = `${this.TWITTER_ENDPOINT}/${user}`;
    this.openBrowser(url);
  }

  getBackgroundImageStyle(url) {
    return this.sanitization.bypassSecurityTrustStyle(`url("${url}")`);
  }

  getMediaGridClassName() {
    let className: string;
    switch (this.mediaList.length) {
      case 2:
        className = 'grid-2';
        break;
      case 3:
        className = 'grid-3';
        break;
      case 4:
        className = 'grid-4';
        break;
      default:
        className = 'grid-1';
        break;
    }
    return className;
  }

  getGridItemClassName(index) {
    if (index > 3) {
      index = 3;
    }
    return `grid-item-${index + 1}`;
  }

  clickMedia(media) {
    this.expandMedia.emit(media);
  }

  postFavorites() {
    if (this.tweet.favorited) {
      this.tweet.favorited = false;
      this.tweet.favorite_count--;
    } else {
      this.tweet.favorited = true;
      this.tweet.favorite_count++;
    }
    this.twitterClient.postFavorites(this.tweet.id_str, this.tweet.favorited)
      .catch((err) => {
        console.log('=== post favorites err ===', err);
        console.log(this.tweet.id_str);
        console.log(this.tweet.favorited);
        if (this.tweet.favorited) {
          this.tweet.favorited = false;
          this.tweet.favorite_count--;
        } else {
          this.tweet.favorited = true;
          this.tweet.favorite_count++;
        }
      });
  }
}
