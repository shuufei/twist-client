import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from 'ionic-angular';
import { TripperHeaderComponent } from './tripper-header/tripper-header';
import { TweetComponent } from './tweet/tweet';
import { MediaPopupComponent } from './media-popup/media-popup';
import { FilterCapsuleButtonComponent } from './filter-capsule-button/filter-capsule-button';
import { PageUpButtonComponent } from './page-up-button/page-up-button';
import { PageDownButtonComponent } from './page-down-button/page-down-button';
@NgModule({
	declarations: [
		TripperHeaderComponent,
    TweetComponent,
    MediaPopupComponent,
    FilterCapsuleButtonComponent,
    PageUpButtonComponent,
    PageDownButtonComponent
	],
	imports: [
		BrowserModule,
		IonicModule
	],
	exports: [
		TripperHeaderComponent,
    TweetComponent,
    MediaPopupComponent,
    FilterCapsuleButtonComponent,
    PageUpButtonComponent,
    PageDownButtonComponent
	]
})
export class ComponentsModule {}
